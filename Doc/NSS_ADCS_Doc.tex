\documentclass[10pt]{article}

\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{indentfirst}
\setlength{\parskip}{0.8em}
\usepackage{listings}
\lstset{
  language=bash,
  basicstyle=\ttfamily
}

\begin{document}

\title{\vspace{-1cm} NSS - ADCS Module Documentation}
\date{\vspace{-1.5cm}August 2019}
\maketitle

\vspace{-1cm}
\section{Introduction}

This module shall be used to evaluate the control feasibility of a satellite mission taking into account its orbital parameters, activity profile, stabilization requirements, actuator and sensor characteristics.

This module consists of a single script that outputs an AEM file (Attitude Ephemeris Message) for a given input orbit expressed in a OEM file (Orbit Ephemeris Message) and a given input Activity Profile, expressed in MEM and MPM files (Mission Ephemeris Message, Mission Parameter Message). These AEM files may later be used as control inputs for satellite maneuvers in a 6DOF simulator to produce an accurate attitude response. The output AEM and OEM Files may also be visualised using VTS.

The feasibility study consists in analysing the parameters of on-board actuators and sensors (performance, geometry, uncertainty) and checking if the current configuration is capable of achieving the mission requirements. This module may serve as a design tool in the phase 0 of a satellite mission.

\section{Getting Started}

To run a full example make sure you have JavaSE-11 installed.

\begin{lstlisting}
  $ sudo add-apt-repository ppa:linuxuprising/java
  $ sudo apt update
  $ sudo apt install oracle-java11-installer
  $ sudo apt install oracle-java11-set-default
\end{lstlisting}

Clone the repository and run:

\begin{lstlisting}
  $ git clone https://sourceforge.isae.fr/git/nss_adcs
  $ cd DEMO
  $ java -jar NSS_ADCS.jar NSS_ADCS_CONFIG.txt
\end{lstlisting}

After some seconds of running, the mission defined in the input files is accepted as feasible and two output files are created with the names chosen in the configuration file. The AEM file may be directly used in VTS in conjunction with the input OEM file to visualize the satellite attitude in orbit. The sensor trust file has information about the attitude determination covariance, according to sensor geometry and uncertainty. For each sensor, this file has values that may be plugged into VTS to visualize a trust region representing the accuracy of a given pointing state. A debug file with information about satellite spin, actuator torques, stored angular momentum, and external torque perturbations is also created as output.

\section{Functionality}

After reading and pre-processing the data from the various input files. The main time loop is initiated, beginning at the starting time of the first mode in the sequence of the Activity Profile. Time is incremented in user-specified time-steps. The mode sequence, specified in the Activity Profile dictates when a mode transition occurs and the time taken to perform that transition.

Each mode is separately defined in its own mode definition file. These mode files contain the required parameters used to create a pointing request. A pointing request specifies the target(s) that should be tracked, independently of the satellite's position, and the spacecraft axis that should track them.

An attitude, expressed in quaternion notation, is then calculated using the position coordinates from the input OEM File and the pointing request at a given time. This attitude is exported directly in AEM format.

The external torque perturbations are calculated using the position, attitude and sensitivity to perturbations of the spacecraft, with the latter being expressed in an input file. Using these perturbations, the rotational state to the spacecraft and an inertial model the control input can be calculated using rigid body mechanics. 

The control torque and stored angular momentum of each actuator is then compared to the maximum values allowed. The mission is declared feasible if the control input always respects the limits.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{Diag.png}
\caption{Schematic representation of the module script. \\ Blue - Data from/to IO Files || Orange - Internal Data}
\label{plane}
\end{figure}

An idealized attitude calculation was chosen because it permits a much faster data processing than a 6DoF simulation would. This module can process data in speeds about 10000x real time in an i7 laptop, with a calculation time step of 5 seconds. This is unmatched by any kind of 6DoF approach, which requires time steps much smaller than the maneuver's time scale. 

\subsection{Attitude Request Definition}

Each satellite mode is composed of a single pointing request. The pointing request consists of a main target and a direction, expressed in the spacecraft's frame, that should be aligned with the main target. The main target can be a celestial object, a location in the central body's surface or a general direction expressed in a specified coordinate frame. The direction in the spacecraft's frame might be the direction of a particular sensor, solar panel, camera or antenna and is expressed as a 3 dimensional vector. A second axis spacecraft axis is then chosen, that shall be aligned as best as possible with a secondary target, such that the secondary target is contained in the plane defined by the two chosen spacecraft axis.

Another type of attitude request consists of a constant spin orbit around a specified axis in the spacecraft's frame with a given angular velocity. This rotation starts at the last computed attitude from the previous mode.

The transition between modes occurs within the time interval specified in the activity profile, starting at the same time as the mode change. During this transition, the attitudes follow a linear interpolation of the attitude at the time of the mode change and the attitude at the end of the transition time, given by the following mode. The user may also specify the acceleration and deceleration time respectively at the beginning and end of this transition maneuver. Shorter transition, acceleration and deceleration times demand more angular momentum storage and torque from the inertia wheels and magnetorquers. The acceleration and deceleration times may be much shorter than the calculation time step but the transition time must be at least the same size to obtain realistic results.

\section{User Interface}

All the interface with the user is done through text files. The OEM and AEM files follow the CIC Exchange Protocol whose documentation is present in the "Doc" folder. This assures these files are compatible with VTS to allow attitude and orbit visualization. All other IO files follow a structure simililar to the CIC MEM and MPM files (Mission Ephemeris Message, Mission Parameter Message) but were adapted due to limitations of these formats. Particullarly, to specify the different modes, sensor, actuator and spacecraft data, multiple parameters were required to be transmitted in a single file, which is not possible following the CIC protocol.

As an alternative all the CIC files were abstracted and they are parsed, treated and written equally. It was considered that a CIC file consists of a header section, with a list of key-value pairs, a metadata section with a list of key-value pairs and finally a data section with a list of string arrays. These section are separated by the words "META START" and "META STOP". The key-value pairs are specified with an "=" sign. Empty lines and lines starting with COMMENT are ignored. This simplified the development of the module, which uses a single file parser/writer for every file type. The keys used throughout the files are fixed, only corresponding values should be changed. 

What distinguishes the file types is the kind of data that is written in them. The AEM and OEM files follow the CIC protocol, MEM files contain temporal information, like mode changes, and MPM files contain static information, like mission or spacecraft parameters.

The spacecraft's frame of reference, used in most of the IO files, can be any frame fixed to and rotating with the aircraft, as long as it is consistent across all files. This is the frame that is used to compute the attitude quaternions and the covariance matrix.

To represent dates and time, only the ISO epoch representation is supported because Patrius doesn't support the modified Julian Date numbers described in the CIC protocol. The time scales that may be used in the files are the ones supported by Patrius:

https://patrius.cnes.fr/uploads/JavaDocs/V4.3/fr/cnes/sirius/patrius/time/TimeScalesFactory.html

The different conventions for each file shall be discussed in the next sections.

\subsection{Configuration File}

The configuration file includes the IO file names and paths written in key-value pairs. The files for mode definition are in the data section as a list, allowing for as many modes as desired by the user, with one file name per data line. The selection of the configuration file is done through the argument of the script when launching it and it should be placed in the base directory of the project.

Besides the IO file names, the configuration also contains other parameters. The key "ATTITUDE\_TIME\_STEP" is used to set the time step of the calculations in seconds. This value doesn't need to be consistent with the time step of the OEM input file nor with the time stamps in the activity profile's mode changes. The key "TRUST\_REGION\_SIGMA\_LEVEL" is used to set the confidence level when creating the trust region for sensor orientation. This is further explained in the Sensor Trust File subsection.


\subsection{Activity Profile}

The metadata section of the activity profile specifies the start/end time of the simulation as well as the time scale in which it is expressed. Due to the interpolation of the position ephemeris in the OEM files, this time interval should be smaller than the time interval of the OEM file, such that the first and last n entries in the OEM file are outside the simulated time interval, where n is the interpolation order of the OEM file.

The ephemeris section of the activity profile consists of five parameters. The first is the starting time, expressed in the chosen timescale, the second is the mode name and the last three are the transition time between consecutive modes, the acceleration and the deceleration period, in seconds.

\subsection{Mode Definition Files}

All the data in a Mode Definition file is contained in the metadata section and consists of key-value pairs. The parameters for an attitude request are expressed in these files an follow the structure described in the "Attitude Request Definition" subsection.

The value of "MODE\_NAME" should correspond to the mode name used in the activity profile. The keys "PRIMARY\_LAW\_TYPE" and "SPACECRAFT\_PRIMARY\_TARGET\_AXIS\_X" (Y,Z) are required to be filled. 

Primary Law type can be of four kinds, ground pointing, body pointing, constant pointing and spin stabilized:

"GROUND\_POINTING" is used to point to a specific location in earth's surface. It is followed by the specification of that point using the keys "PRIMARY\_TARGET\_LON" (LAT,ALT), whose values should be in degrees and meters respectively.

"BODY\_POINTING" is used to point to a specific location in earth's surface. It is followed by the specification of the body using the key "PRIMARY\_TARGET". The available bodies are the ones supported by Patrius:

https://patrius.cnes.fr/uploads/JavaDocs/V4.3/fr/cnes/sirius/patrius/bodies/CelestialBodyFactory.html

"CONSTANT\_POINTING" is used to specify a constant direction in a single frame, inertial or not. Is is followed by the specification of the direction using the keys "PRIMARY\_TARGET\_X" (Y,Z) and of the frame using the key "PRIMARY\_TARGET\_FRAME". The available frames are the ones supported by Patrius, predefined and local orbital frames:

https://patrius.cnes.fr/uploads/JavaDocs/V4.3/fr/cnes/sirius/patrius/frames/Predefined.html

https://patrius.cnes.fr/uploads/JavaDocs/V4.3/fr/cnes/sirius/patrius/frames/LOFType.html

"SPIN\_STABILIZED" is used to spin around a fixed spacecraft axis and is followed by the specification of the angular velocity using the key "SPIN\_RATE" whose value should be in radians per second. 

The values for the spacecraft primary axis and for the constant pointing are unitless since they represent a direction. The vector doesn't need to normalized either.

In the case of a spin stabilized primary law, no secondary law is necessary. In all other cases the secondary law type can be one of the following three, ground pointing, body pointing and constant pointing, with the same required parameters. The keys change only by substituting the word "PRIMARY" by "SECONDARY".

Example files are included in the "DEMO" folder and may altered to create new mission specifications.


\subsection{Actuators File}

The actuator characteristics are defined in the data section of the actuators .mpm file. Each actuator is defined in its own data line where the second field is the actuator ID. This ID is used to report when an actuators exceeds its capacity at a given instant in time. The third field is the the type of actuator, the values for this field can be either "INERTIA\_WHEEL" or "MAGNETORQUER". The subsequent three fields are the unnormalized direction of the actuator, represented in the spacecraft's frame of reference. In the case of an inertia wheel, it's its axis of rotation. In the case of a magnetorquer, it's the direction of the generated magnetic dipole. The next two values for an inertia wheel are its maximum stored momentum and its maximum torque, respectively represented in $[kg \cdot m^2 \cdot s^{-1}]$ and $[kg \cdot m^2 \cdot s^{-2}]$. The last field for the magnetorquer is its maximum magnetic dipole expressed in $[A \cdot m^2]$.

\subsection{Sensors File}

The sensor characteristics are defined in the data section of the sensors .mpm file. Each sensor is defined in its own data line where the second field is the actuator ID. The subsequent three fields are the unnormalized direction of the sensor, represented in the spacecraft's frame of reference, around which its uncertainty is measured. The last value is the standard deviation (uncertainty) of the measurement, represented in radians.

\subsection{Sensor Trust File}

Using the direction and uncertainty values from each sensor, it is possible to calculate the total covariance matrix, which expresses the total uncertainty in the orientation of the spacecraft. This is done using a covariance analysis of Wahba’s Problem described in section 5.5.2 of:

F.L. Markley and J.L. Crassidis, Fundamentals of Spacecraft Attitude Determination
and Control, Space Technology Library 33, DOI 10.1007/978-1-4939-0802-8,
© Springer Science+Business Media New York 2014

This matrix is then described in the frame of reference of each of the sensors, allowing the calculation of a trust region for the orientation of each sensor. This trust region consists of an elliptic cone with a shape and orientation given by the values of its half-angle in x, y, and its orientation with the rotations, Rz, Ry, Rx in the spacecraft's frame. These values can be copied directly into VTS sensor properties and orientation. (by choosing the euler angle representation with a 321 rotation sequence). This will display the trust region in VTS's display applications. The size of the elliptic region is determined in the configuration file as a multiple of the standard deviation (real number). The higher this number, the bigger the ellipse becomes and more likely it is that the sensor direction is inside the trust region.

\subsection{Inertial Model File}

The inertial model file serves to input the spacecraft mass distribution characteristics. In the metadata section the keys "TOTAL\_MASS" and "MASS\_CENTER\_X" (Y,Z) indicate the total satellite mass in $[kg]$ and the mass center vector in the spacecraft's frame of reference in meters. The data section includes three data lines expressing the moment of inertia matrix in $[kg \cdot m^2]$.

\subsection{Torque Perturbation Sensitivity File}

This file defines the spacecraft's sensitivity to aerodynamic, magnetic and radiation torque perturbations. Gravitational torque perturbations stem from the spacecraft's inertial model, covered previously. 

The keys "AERO\_CENTER\_X" (Y,Z) indicate the aerodynamic pressure center, expressed in meters in the spacecraft's frame. The keys "AERO\_CX\_S" (CY,CZ) reflect the aerodynamic coefficients (unitless) multiplied by the projected surface $[m^2]$ in each of the spacecraft's axis. The aerodynamic torque is calculated by taking the satellite's velocity relative to the atmosphere, which also rotates with earth, and calculating a vectorial radiation pressure, colinear with the relative velocity, $\mathbf{p} = - 0.5 \cdot \rho \cdot V_r \cdot \mathbf{V_r}$. Each component of this radiation pressure is scaled by the respective pressure coefficient and projected surface to calculate the total drag force, $F_i = p_i \cdot C_i \cdot S_i$. The torque is then computed using the pressure center as the moment arm.

The keys "MAG\_DIPOLE\_X" (Y,Z) define the base magnetic dipole of the spacecraft, excluding the contribution of the magnetorquers. The magnetic dipole should be expressed in the spacecraft's frame in $[A \cdot m^2]$. The magnetic torque is computed using the magnetic moment formula: $\mathbf{\tau} = \mathbf{m} \times \mathbf{B}$ where $\mathbf{m}$ is the satellite's magnetic dipole and $\mathbf{B}$ is earth's magnetic field at the satellite's position, all expressed in the spacecraft's frame of reference.

Cullity, B. D.; Graham, C. D. (2008). Introduction to Magnetic Materials (2nd ed.). Wiley-IEEE Press. p. 103. ISBN 978-0-471-47741-9.

The keys "RAD\_PRESSURE\_CENTER\_X" (Y,Z) specify the radiation pressure center in the spacecraft's frame of reference in meters. The keys "RAD\_FORCE\_X" (Y,Z) represent the solar radiation pressure multiplied by the projected surface in each of the spacecraft's axis and should be expressed in micro Newtons. The radiation induced torque is calculated in a similar manner to the aerodynamic torque.

\subsection{Report File}

After the script's execution, a report file is created. If this file is empty, the mission is completely feasible. 

If a message of the type "Magnetorquer MT1 can't cope with external torque at date: 2010-01-01T12:02:04.000" is shown, it means that the mission is still feasible but at the specified timestamp, the satellite might not be able to perform momentum dumping due to a lack of magnetorquer effectiveness. This can be caused by a misalignment with the earths magnetic field.

If a message indicating that one or multiple inertia wheels exceeded their torque or momentum storage capacity, then the mission is declared unfeasible.

\subsection{Debug File}

The debug file consists of the values for multiple parameters at every calculation step. It is organized in groups of 5 data lines for every time step. All these quantities are expressed in the spacecraft's frame of reference. The first one is the date and time. 

The second line has the 3D vector values for the angular velocity, in radians per second, the angular momentum, in $[kg \cdot m^2 \cdot s^{-1}]$, and the total control torque required from the actuators, in $[kg \cdot m^2 \cdot s^{-2}]$. 

The third line has the control allocation for each of the n inertial wheels and the m magnetorquers. The first n values are the control torques for the inertia wheels, the next n values are the angular momentum of each inertia wheel, and finally the last m values are the control torques of the magnetorquers necessary to cancel out the external torque perturbations. These values are adimentionalized by the maximum capacity of each of the actuators. This means that values in the range from -1 to 1 are considered acceptable whereas values outside this interval imply that the actuator has exceeded its performance limits, generating a message in the report file.

The fourth line has the 3D vector torque perturbations of each type, magnetic, aerodynamic, radiation and gravitational, in this order, in $[kg \cdot m^2 \cdot s^{-2}]$. The last line consists of the sum of all the perturbation torques. 

\end{document}

