package adcs;

import fr.cnes.sirius.patrius.time.AbsoluteDate;
import fr.cnes.sirius.patrius.time.DateTimeComponents;

public class SatelliteMode {

	private AbsoluteDate modeStart;
	private AbsoluteDate transitionEnd;
	private AbsoluteDate accelerationEnd;
	private AbsoluteDate decelerationEnd;
	private AbsoluteDate modeEnd;
	private String modeName;
	private double transitionTime;
	private double accelerationTime;
	private double decelerationTime;

	public SatelliteMode(String modeStart, String modeEnd, double transitionTime,  double accelerationTime,  double decelerationTime, String timeScale, String modeName) {
		this.modeStart = new AbsoluteDate(DateTimeComponents.parseDateTime(modeStart), FactoryUtil.getTimeScale(timeScale));
		this.modeEnd = new AbsoluteDate(DateTimeComponents.parseDateTime(modeEnd), FactoryUtil.getTimeScale(timeScale));
		this.modeName = modeName;
		this.transitionTime = transitionTime;
		this.transitionEnd = this.modeStart.shiftedBy(transitionTime);
		this.accelerationTime = accelerationTime;
		this.decelerationTime = decelerationTime;
		this.accelerationEnd = this.modeStart.shiftedBy(accelerationTime);
		this.decelerationEnd = this.transitionEnd.shiftedBy(decelerationTime);
	}	
	
	public double getTransitionTime() {
		return transitionTime;
	}
	
	public double getAccelerationTime() {
		return accelerationTime;
	}

	public double getDecelerationTime() {
		return decelerationTime;
	}

	public boolean isFinished(AbsoluteDate currentDate) {
		return (currentDate.compareTo(modeEnd) >= 0);
	}
	
	public boolean isStarted(AbsoluteDate currentDate) {
		return (currentDate.compareTo(modeStart) >= 0);
	}
	
	public boolean isInTransition(AbsoluteDate currentDate) {
		return (currentDate.compareTo(modeStart) >= 0 && currentDate.compareTo(transitionEnd) <= 0 && transitionTime != 0);
	}
	
	public boolean isAccelerating(AbsoluteDate currentDate) {
		return (currentDate.compareTo(modeStart) >= 0 && currentDate.compareTo(accelerationEnd) <= 0 && accelerationTime != 0);
	}
	
	public boolean isDecelerating(AbsoluteDate currentDate) {
		return (currentDate.compareTo(transitionEnd) >= 0 && currentDate.compareTo(decelerationEnd) <= 0 && decelerationTime != 0);
	}

	public String getModeName() {
		return modeName;
	}
	
	public AbsoluteDate getModeStart() {
		return modeStart;
	}

	public AbsoluteDate getModeEnd() {
		return modeEnd;
	}
	
	public AbsoluteDate getTransitionEnd() {
		return transitionEnd;
	}

}
