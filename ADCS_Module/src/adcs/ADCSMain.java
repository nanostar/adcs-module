package adcs;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import fr.cnes.sirius.addons.patriusdataset.PatriusDataset;
import fr.cnes.sirius.patrius.attitudes.Attitude;
import fr.cnes.sirius.patrius.attitudes.AttitudeProvider;
import fr.cnes.sirius.patrius.attitudes.ConstantSpinSlew;
import fr.cnes.sirius.patrius.frames.Frame;
import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.Rotation;
import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.RotationOrder;
import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.Vector3D;
import fr.cnes.sirius.patrius.math.geometry.euclidean.twod.Vector2D;
import fr.cnes.sirius.patrius.math.linear.Array2DRowRealMatrix;
import fr.cnes.sirius.patrius.math.linear.EigenDecomposition;
import fr.cnes.sirius.patrius.math.linear.RealMatrix;
import fr.cnes.sirius.patrius.math.linear.RealVector;
import fr.cnes.sirius.patrius.math.linear.SingularValueDecomposition;
import fr.cnes.sirius.patrius.math.util.FastMath;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.EphemerisPvHermite;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.PVCoordinates;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.PVCoordinatesProvider;
import fr.cnes.sirius.patrius.time.AbsoluteDate;
import fr.cnes.sirius.patrius.time.DateTimeComponents;
import fr.cnes.sirius.patrius.time.TimeScale;
import fr.cnes.sirius.patrius.utils.exception.PatriusException;

public class ADCSMain {

	public static void main(String[] args) throws PatriusException {
		
		// Patrius Dataset initialization (needed for example to get the UTC time)
        PatriusDataset.addResourcesFromPatriusDataset() ;
        
        // Read config file
        CICFile config = new CICFile(args[0],"");
        LinkedHashMap<String,String> ioFiles = config.getMetaData();        
        ArrayList<String[]> modeFileNames = config.getData();
              
        // Read OEM File and orbit interpolation
        CICFile orbitEphemeris = new CICFile(ioFiles.get("OEM_INPUT_FILE"),ioFiles.get("OEM_INPUT_PATH"));	
		ArrayList<String[]> ephems = orbitEphemeris.getData();
		PVCoordinates[] coords = new PVCoordinates[ephems.size()];
		AbsoluteDate[] dates = new AbsoluteDate[ephems.size()];		
		Frame oemFrame = FactoryUtil.getTargetFrame(orbitEphemeris.getMetaData().get("REF_FRAME"), null, null);

		TimeScale timeScale = FactoryUtil.getTimeScale(orbitEphemeris.getMetaData().get("TIME_SYSTEM"));
		for (int i=0; i<ephems.size(); i++) {
			String[] ephem =  ephems.get(i);
			Vector3D position = new Vector3D(Double.parseDouble(ephem[1]),Double.parseDouble(ephem[2]),Double.parseDouble(ephem[3])).scalarMultiply(1e3);
			Vector3D velocity = new Vector3D(Double.parseDouble(ephem[4]),Double.parseDouble(ephem[5]),Double.parseDouble(ephem[6])).scalarMultiply(1e3);

			String dateStr = ephem[0].replace(" ", "T");
			dates[i] = new AbsoluteDate(DateTimeComponents.parseDateTime(dateStr), timeScale);
			coords[i] = new PVCoordinates(position, velocity);
		}
		PVCoordinatesProvider orbit = new EphemerisPvHermite(coords, 2, null, oemFrame, dates, null);
		
		// read Activity Profile
		CICFile activityProfile = new CICFile(ioFiles.get("ACTIVITY_PROFILE_INPUT_FILE"),ioFiles.get("ACTIVITY_PROFILE_INPUT_PATH"));
		ArrayList<String[]> modeSequenceData = activityProfile.getData();
		ArrayList<SatelliteMode> modeSequence = new ArrayList<SatelliteMode>();
		String start, end, modeName;
		double transition, acceleration, deceleration;
		String timeSystem = activityProfile.getMetaData().get("TIME_SYSTEM");
		
		for (int i=0; i<modeSequenceData.size(); i++) {
			start = modeSequenceData.get(i)[0];
			if (i<modeSequenceData.size()-1) {
				end = modeSequenceData.get(i+1)[0];
			} else {
				end = activityProfile.getMetaData().get("STOP_TIME");
			}
			modeName = modeSequenceData.get(i)[1];
			transition = Double.parseDouble(modeSequenceData.get(i)[2]);
			acceleration = Double.parseDouble(modeSequenceData.get(i)[3]);
			deceleration = Double.parseDouble(modeSequenceData.get(i)[4]);
			SatelliteMode mode = new SatelliteMode(start,end, transition, acceleration, deceleration, timeSystem, modeName);
			modeSequence.add(mode);
		}

		// read Mode definition files	
		HashMap<String, AttitudeRequest> modeDefinitionFiles = new HashMap<String, AttitudeRequest>();
		for (String[] fileName : modeFileNames) {				
			CICFile modeFile = new CICFile(fileName[0],ioFiles.get("MODE_DEFINITION_INPUT_PATH"));
			AttitudeRequest request = new AttitudeRequest(modeFile, orbit, oemFrame);
			modeDefinitionFiles.put(modeFile.getMetaData().get("MODE_NAME"), request);
		}
		
		// read inertia matrix
		CICFile inertialModelFile = new CICFile(ioFiles.get("INERTIAL_MODEL_INPUT_FILE"),ioFiles.get("INERTIAL_MODEL_INPUT_PATH"));
		double[][] matrix = new double[3][3];
		for (int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				matrix[i][j] = Double.parseDouble(inertialModelFile.getData().get(i)[j+1]);
			}
		}
		Array2DRowRealMatrix inertiaMatrix = new Array2DRowRealMatrix(matrix);
		
		// read sensor data
		CICFile sensorsFile = new CICFile(ioFiles.get("SENSORS_INPUT_FILE"),ioFiles.get("SENSORS_INPUT_PATH"));
		ArrayList<String[]> sensorsData = sensorsFile.getData();
		ArrayList<Sensor> sensors = new ArrayList<Sensor>();
				
		for (int i=0; i<sensorsData.size(); i++) {
			String[] actuatorLine =  sensorsData.get(i);
			Sensor s = new Sensor(actuatorLine[1],
					actuatorLine[2],
					actuatorLine[3],
					actuatorLine[4],
					actuatorLine[5]);						
			sensors.add(s);		
		}
		
		// compute covariance matrix
		double[][] empty = {{0,0,0},
				{0,0,0},
				{0,0,0}};;
		RealMatrix F = new Array2DRowRealMatrix(empty);
		
		double[][] identity = {{1,0,0},
				{0,1,0},
				{0,0,1}};
		RealMatrix I = new Array2DRowRealMatrix(identity);
		
		for (int i=0; i<sensors.size(); i++) {
			RealMatrix dir = new Array2DRowRealMatrix(sensors.get(i).getDirection().toArray());
			double sigma = sensors.get(i).getSigma();
			F = F.add(I.add(dir.multiply(dir.transpose()).scalarMultiply(-1)).scalarMultiply(1/sigma*1/sigma));
		}
		
		RealMatrix covariance = new SingularValueDecomposition(F).getSolver().getInverse();
		
		// compute sensor trust region
		double sigmaTrust = Double.parseDouble(ioFiles.get("TRUST_REGION_SIGMA_LEVEL"));
		
		CICFile sensorTrustFile = new CICFile();
		ArrayList<String[]> sensorTrustData = new ArrayList<String[]>();
		String[] line0 = {"Total Covariance Matrix:"};
		sensorTrustData.add(line0);
		String[] covarianceLine = {covariance.toString()};
		sensorTrustData.add(covarianceLine);
		String[] emptyLine = {""};
	
		for (int i=0; i<sensors.size(); i++) {
			sensorTrustData.add(emptyLine);
			String[] sensorID = {"SensorID: " + sensors.get(i).getID()};
			sensorTrustData.add(sensorID);
			sensorTrustData.add(emptyLine);

			Vector3D sensorDirection = sensors.get(i).getDirection();
			Vector3D ex = new Vector3D(1, 0, 0);
			Vector3D ez = new Vector3D(0, 0, 1);

			Rotation sensorRotation;
			try {
				sensorRotation = new Rotation(ex, ez, sensorDirection, ez);
			} catch (Exception e){
				sensorRotation = new Rotation(RotationOrder.ZYX, 0, -90, 0);
			}
			RealMatrix sensorDCM = new Array2DRowRealMatrix(sensorRotation.getMatrix());
			RealMatrix covarianceInSensorFrame = sensorDCM.preMultiply(covariance).preMultiply(sensorDCM.transpose());
			
			int[] subIndex = {1, 2};
			RealMatrix axialCovariance = covarianceInSensorFrame.getSubMatrix(subIndex, subIndex);		
			EigenDecomposition ed = new EigenDecomposition(axialCovariance);
			RealVector EigenVector = ed.getEigenvector(0);
			
			double angle = FastMath.acos(EigenVector.cosine(new Vector2D(1, 0).getRealVector()));
			String[] eigenvalues = {"Eigenvalues: Half-angle around X and Y: (degrees) ", Double.toString(FastMath.toDegrees(FastMath.sqrt(ed.getRealEigenvalue(0)))*sigmaTrust),
					Double.toString(FastMath.toDegrees(FastMath.sqrt(ed.getRealEigenvalue(1)))*sigmaTrust)};
			sensorTrustData.add(eigenvalues);
			
			Rotation fullRotation = sensorRotation.applyTo(new Rotation(RotationOrder.ZYX, 0, 0, angle));
			String[] rotations = {"Rz, Ry, Rx: (degrees) ", Double.toString(FastMath.toDegrees(fullRotation.getAngles(RotationOrder.ZYX)[0])),
					Double.toString(FastMath.toDegrees(fullRotation.getAngles(RotationOrder.ZYX)[1])),
					Double.toString(FastMath.toDegrees(fullRotation.getAngles(RotationOrder.ZYX)[2]))};
			sensorTrustData.add(rotations);
		} 
		
		// read actuator data
		CICFile actuatorsFile = new CICFile(ioFiles.get("ACTUATORS_INPUT_FILE"),ioFiles.get("ACTUATORS_INPUT_PATH"));
		ArrayList<String[]> actuatorsData = actuatorsFile.getData();
		ArrayList<InertiaWheel> inertiaWheels = new ArrayList<InertiaWheel>();
		ArrayList<Magnetorquer> magnetorquers = new ArrayList<Magnetorquer>();
		
		for (int i=0; i<actuatorsData.size(); i++) {
			String[] actuatorLine =  actuatorsData.get(i);
			if (actuatorLine[2].contentEquals("INERTIA_WHEEL")) {
				InertiaWheel iw = new InertiaWheel(actuatorLine[1],
						actuatorLine[3],
						actuatorLine[4],
						actuatorLine[5],
						actuatorLine[6],
						actuatorLine[7]);
				
				
				inertiaWheels.add(iw);
			} else if (actuatorLine[2].contentEquals("MAGNETORQUER")) {
				Magnetorquer mt = new Magnetorquer(actuatorLine[1],
						actuatorLine[3],
						actuatorLine[4],
						actuatorLine[5],
						actuatorLine[6]);
				magnetorquers.add(mt);
			}			
		}
		
		double[][] maxMoments = new double[3][inertiaWheels.size()];
		for (int i=0; i<inertiaWheels.size(); i++) {
			Vector3D column = inertiaWheels.get(i).getMaxMoment();
			maxMoments[0][i]=column.getX();
			maxMoments[1][i]=column.getY();
			maxMoments[2][i]=column.getZ();		
		}
		RealMatrix maxMomentsMatrix = new Array2DRowRealMatrix(maxMoments);
		
		double[][] maxTorquesIW = new double[3][inertiaWheels.size()];
		for (int i=0; i<inertiaWheels.size(); i++) {
			Vector3D column = inertiaWheels.get(i).getMaxTorque();
			maxTorquesIW[0][i]=column.getX();
			maxTorquesIW[1][i]=column.getY();
			maxTorquesIW[2][i]=column.getZ();		
		}
		RealMatrix maxTorquesIWMatrix = new Array2DRowRealMatrix(maxTorquesIW);
		
		// Define Header and metadata
		CICFile attitudeEphemeris = new CICFile();
		CICFile debugFile = new CICFile();
		CICFile reportFile = new CICFile();
		CICFile torqueCoeffsFile = new CICFile(ioFiles.get("TORQUES_INPUT_FILE"),ioFiles.get("TORQUES_INPUT_PATH"));
		ExternalTorques extTorques = new ExternalTorques(torqueCoeffsFile, inertialModelFile);

		LinkedHashMap<String,String> header = new LinkedHashMap<String,String>();
		header.put("CIC_AEM_VERS", "1.0");
		header.put("CREATION_DATE", Instant.now().toString());
		header.put("ORIGINATOR", "ISAE SUPAERO - NSS ADCS MODULE");
		
		LinkedHashMap<String,String> metaData = new LinkedHashMap<String,String>();
		metaData.put("OBJECT_NAME", activityProfile.getMetaData().get("OBJECT_NAME"));
		metaData.put("OBJECT_ID", activityProfile.getMetaData().get("OBJECT_ID"));
		metaData.put("CENTER_NAME", orbitEphemeris.getMetaData().get("CENTER_NAME"));
		metaData.put("REF_FRAME_A", orbitEphemeris.getMetaData().get("REF_FRAME"));
		metaData.put("REF_FRAME_B ", "SC_BODY_1");
		metaData.put("ATTITUDE_DIR", "A2B");
		metaData.put("TIME_SYSTEM", activityProfile.getMetaData().get("TIME_SYSTEM"));
		metaData.put("ATTITUDE_TYPE", "QUATERNION");	

		// loop initialization
		ArrayList<String[]> attitudeData = new ArrayList<String[]>();
		ArrayList<String[]> spinData = new ArrayList<String[]>();
		ArrayList<String[]> reportData = new ArrayList<String[]>();
		int modeCounter = 0;
		SatelliteMode currentMode = modeSequence.get(modeCounter);
		SatelliteMode previousMode = currentMode;
		AbsoluteDate date = currentMode.getModeStart();		
		AttitudeRequest currentRequest = modeDefinitionFiles.get(currentMode.getModeName());
		AttitudeRequest previousRequest;
		AttitudeProvider currentAttitudeLaw = currentRequest.getAttitudeLaw(null);
		AttitudeProvider previousAttitudeLaw;
		Attitude currentAttitude = currentAttitudeLaw.getAttitude(orbit, date, oemFrame);
		Attitude previousAttitude = currentAttitude;
		Attitude finalAttitude = null;
		ConstantSpinSlew attitudeSlewLaw;
		String q0, q1, q2, q3;
		String dateString;
		boolean feasibility = true;
		double timeStep = Double.parseDouble(ioFiles.get("ATTITUDE_TIME_STEP")); //seconds

		System.out.println("Running... ");
		while(modeCounter<modeSequence.size()) {
			
			// Attitude calculation
			currentMode = modeSequence.get(modeCounter);			
			currentRequest = modeDefinitionFiles.get(currentMode.getModeName());
			currentAttitudeLaw = currentRequest.getAttitudeLaw(finalAttitude);
			previousAttitude = currentAttitude;
			
			if (currentMode.isInTransition(date)) {
				previousRequest = modeDefinitionFiles.get(previousMode.getModeName());
				previousAttitudeLaw = previousRequest.getAttitudeLaw(finalAttitude);		
				attitudeSlewLaw = new ConstantSpinSlew(previousAttitudeLaw, currentAttitudeLaw, currentMode.getModeStart(), currentMode.getTransitionEnd());
				attitudeSlewLaw.compute(orbit);
				currentAttitude = attitudeSlewLaw.getAttitude(orbit, date, oemFrame);		
			} else {
				currentAttitude = currentAttitudeLaw.getAttitude(orbit, date, oemFrame);
			}
					
			q0 = String.format("%.12f",currentAttitude.getRotation().getQuaternion().getQ0());
			q1 = String.format("%.12f",currentAttitude.getRotation().getQuaternion().getQ1());
			q2 = String.format("%.12f",currentAttitude.getRotation().getQuaternion().getQ2());
			q3 = String.format("%.12f",currentAttitude.getRotation().getQuaternion().getQ3());
			dateString = date.toString(FactoryUtil.getTimeScale(timeSystem));
			String[] dataLine = {dateString, q0, q1, q2, q3};
			attitudeData.add(dataLine);
			
			// Internal Torques
			Vector3D spin = currentAttitude.getSpin();
			double[] spinArray = new double[3];
			spinArray[0] = spin.getX();
			spinArray[1] = spin.getY();
			spinArray[2] = spin.getZ();
			
			RealMatrix spinVector = new Array2DRowRealMatrix(spinArray);			
			RealMatrix angularMomentum = inertiaMatrix.multiply(spinVector);		
			RealMatrix deltaSpin = spinVector.subtract(new Array2DRowRealMatrix(previousAttitude.getSpin().getRealVector().toArray()));
			RealMatrix angularAccel;
			
			if (currentMode.isAccelerating(date)) {
				angularAccel = deltaSpin.scalarMultiply(1/currentMode.getAccelerationTime());
			} else if (currentMode.isDecelerating(date)) {
				angularAccel = deltaSpin.scalarMultiply(1/currentMode.getDecelerationTime());
			} else {
				angularAccel = deltaSpin.scalarMultiply(1/timeStep);
			}
			
			RealMatrix internalTorque = inertiaMatrix.multiply(angularAccel)
					.add(new Array2DRowRealMatrix(Vector3D.crossProduct(spin, 
							new Vector3D(angularMomentum.getColumn(0))).getRealVector().toArray()));
			
			// External Torques
						extTorques.setState(orbit, oemFrame, date, currentAttitude);
						
						Vector3D totalExternalTorques = extTorques.getMagTorque()
								.add(extTorques.getAeroTorque())
								.add(extTorques.getRadTorque())
								.add(extTorques.getGravTorque());		
						RealMatrix externalTorques = new Array2DRowRealMatrix(totalExternalTorques.toArray());
			
			// Sum
			RealMatrix totalTorque = internalTorque.add(externalTorques);
			
			// Control allocation			
			double[][] maxTorquesMT = new double[3][magnetorquers.size()];
			for (int i=0; i<magnetorquers.size(); i++) {
				magnetorquers.get(i).setState(orbit, oemFrame, date, currentAttitude);
				Vector3D column = magnetorquers.get(i).getMaxTorque();
				maxTorquesMT[0][i]=column.getX();
				maxTorquesMT[1][i]=column.getY();
				maxTorquesMT[2][i]=column.getZ();		
			}			
			RealMatrix maxTorquesMTMatrix = new Array2DRowRealMatrix(maxTorquesMT);			
			
			RealMatrix inverseMaxMoments = new SingularValueDecomposition(maxMomentsMatrix).getSolver().getInverse();
			RealMatrix controlVectorIW = inverseMaxMoments.multiply(angularMomentum);
			
			RealMatrix inverseMaxTorquesIW = new SingularValueDecomposition(maxTorquesIWMatrix).getSolver().getInverse();
			RealMatrix controlVectorTorquesIW = inverseMaxTorquesIW.multiply(totalTorque);
			
			RealMatrix inverseMaxTorquesMTExt = new SingularValueDecomposition(maxTorquesMTMatrix).getSolver().getInverse();		
			RealMatrix controlVectorTorquesMTExt = inverseMaxTorquesMTExt.multiply(externalTorques);	
			
			// Debug Output
			String[] lineDate = {dateString};
			spinData.add(lineDate);

			String[] line1 = {
					String.format("%.10f",spinArray[0]), 
					String.format("%.10f",spinArray[1]),
					String.format("%.10f",spinArray[2]),
					String.format("%.10f",angularMomentum.getColumn(0)[0]),
					String.format("%.10f",angularMomentum.getColumn(0)[1]),
					String.format("%.10f",angularMomentum.getColumn(0)[2]),
					String.format("%.10f",totalTorque.getColumn(0)[0]),
					String.format("%.10f",totalTorque.getColumn(0)[1]),
					String.format("%.10f",totalTorque.getColumn(0)[2])};
			spinData.add(line1);

			int nIW = controlVectorTorquesIW.getRowDimension();
			int nMT = controlVectorTorquesMTExt.getRowDimension();
			String[] line2 = new String[2*nIW+nMT];
			for (int i=0; i<nIW; i++) {
				line2[i] = String.format("%.10f",controlVectorTorquesIW.getColumn(0)[i]);
				line2[i+nIW] = String.format("%.10f",controlVectorIW.getColumn(0)[i]);
			}
			for (int i=0; i<nMT; i++) {
				line2[i+2*nIW] = String.format("%.10f",controlVectorTorquesMTExt.getColumn(0)[i]);
			}
			spinData.add(line2);

			String[] line3 = {
					String.format("%.10f",extTorques.getMagTorque().getX()),
					String.format("%.10f",extTorques.getMagTorque().getY()),
					String.format("%.10f",extTorques.getMagTorque().getZ()),
					String.format("%.10f",extTorques.getAeroTorque().getX()),
					String.format("%.10f",extTorques.getAeroTorque().getY()),
					String.format("%.10f",extTorques.getAeroTorque().getZ()),
					String.format("%.10f",extTorques.getRadTorque().getX()),
					String.format("%.10f",extTorques.getRadTorque().getY()),
					String.format("%.10f",extTorques.getRadTorque().getZ()),
					String.format("%.10f",extTorques.getGravTorque().getX()),
					String.format("%.10f",extTorques.getGravTorque().getY()),
					String.format("%.10f",extTorques.getGravTorque().getZ())};
			spinData.add(line3);

			String[] line4 = {
					String.format("%.10f",totalExternalTorques.getX()),
					String.format("%.10f",totalExternalTorques.getY()),
					String.format("%.10f",totalExternalTorques.getZ())};
			spinData.add(line4);

			// Mission Feasibility Report
			for (int i=0; i<controlVectorIW.getRowDimension(); i++) {
				if (controlVectorIW.getColumn(0)[i] >= 1 || controlVectorIW.getColumn(0)[i] <= -1) {
					feasibility = false;
					String[] warning = {"Inertia Wheel " + inertiaWheels.get(i).getID() + " exceeds angular momuentum storage capacity at date: " + dateString};
					reportData.add(warning);
				}
				if (controlVectorTorquesIW.getColumn(0)[i] >= 1 || controlVectorTorquesIW.getColumn(0)[i] <= -1) {
					feasibility = false;
					String[] warning = {"Inertia Wheel "  + inertiaWheels.get(i).getID() + " exceeds torque capacity at date: " + dateString};
					reportData.add(warning);					
				}			
			}
			
			for (int i=0; i<controlVectorTorquesMTExt.getRowDimension(); i++) {
				
				if (controlVectorTorquesMTExt.getColumn(0)[i] >= 1 || controlVectorTorquesMTExt.getColumn(0)[i] <= -1) {
					String[] warning = {"Magnetorquer " + magnetorquers.get(i).getID() + " can't cope with external torque at date: " + dateString + " (inertia wheel momentum dumping might not be possible at this stage)"};
					reportData.add(warning);
				}
			}

			// Loop increment and mode Switching
			date = date.shiftedBy(timeStep);
			if (currentMode.isFinished(date)) {
				finalAttitude = currentAttitude;
				previousMode = currentMode;
				modeCounter++;
			}
			
		};
		
		// Write to Files
		
		LinkedHashMap<String,String> emptyMetaData = new LinkedHashMap<String,String>();
		
		debugFile.setHeader(header);
		debugFile.setMetaData(emptyMetaData);
		debugFile.setData(spinData);
		debugFile.writeToFile(ioFiles.get("DEBUG_OUTPUT_FILE"),ioFiles.get("DEBUG_OUTPUT_PATH"));
		
		reportFile.setHeader(header);
		reportFile.setMetaData(emptyMetaData);
		reportFile.setData(reportData);
		reportFile.writeToFile(ioFiles.get("REPORT_OUTPUT_FILE"),ioFiles.get("REPORT_OUTPUT_PATH"));
		
		sensorTrustFile.setHeader(header);
		sensorTrustFile.setMetaData(emptyMetaData);
		sensorTrustFile.setData(sensorTrustData);
		sensorTrustFile.writeToFile(ioFiles.get("SENSOR_TRUST_OUTPUT_FILE"),ioFiles.get("SENSOR_TRUST_OUTPUT_PATH"));
		
		attitudeEphemeris.setHeader(header);
		attitudeEphemeris.setMetaData(metaData);
		attitudeEphemeris.setData(attitudeData);
		attitudeEphemeris.writeToFile(ioFiles.get("AEM_OUTPUT_FILE"),ioFiles.get("AEM_OUTPUT_PATH"));
		
		System.out.println("Mission is Feasible: " + feasibility);
	
	}
}
