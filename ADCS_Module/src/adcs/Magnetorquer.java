package adcs;

import fr.cnes.sirius.patrius.attitudes.Attitude;
import fr.cnes.sirius.patrius.bodies.GeodeticPoint;
import fr.cnes.sirius.patrius.bodies.OneAxisEllipsoid;
import fr.cnes.sirius.patrius.frames.Frame;
import fr.cnes.sirius.patrius.frames.FramesFactory;
import fr.cnes.sirius.patrius.frames.TopocentricFrame;
import fr.cnes.sirius.patrius.frames.transformations.Transform;
import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.Vector3D;
import fr.cnes.sirius.patrius.models.earth.GeoMagneticElements;
import fr.cnes.sirius.patrius.models.earth.GeoMagneticField;
import fr.cnes.sirius.patrius.models.earth.GeoMagneticFieldFactory;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.PVCoordinatesProvider;
import fr.cnes.sirius.patrius.time.AbsoluteDate;
import fr.cnes.sirius.patrius.utils.Constants;
import fr.cnes.sirius.patrius.utils.exception.PatriusException;

public class Magnetorquer {
	
	private String ID;
	private Vector3D direction;
	private double maxMagTorque;
	private PVCoordinatesProvider orbit;
	private Frame frame;
	private AbsoluteDate date;
	private Attitude attitude;
	private GeoMagneticField magModel;
	
	public Magnetorquer(String ID, String dirX, String dirY, String dirZ, String maxMagTorque) {
		this.ID = ID;
		direction = new Vector3D(Double.parseDouble(dirX),
				Double.parseDouble(dirY),
				Double.parseDouble(dirZ)).normalize();
		this.maxMagTorque = Double.parseDouble(maxMagTorque);
		
		// get magnetic model
		try {
			magModel = GeoMagneticFieldFactory.getIGRF(2018);
		} catch (PatriusException e) {
			e.printStackTrace();
		}
	}
	
	public void setState(PVCoordinatesProvider orbit, Frame frame, AbsoluteDate date, Attitude attitude) {
		this.orbit = orbit;
		this.frame = frame;
		this.date = date;
		this.attitude = attitude;
	}

	public Vector3D getMaxTorque() {
				
		try {
			
			Frame ITRF = FramesFactory.getITRF();
			double earthRadius = Constants.WGS84_EARTH_EQUATORIAL_RADIUS;
			OneAxisEllipsoid earthShape = new OneAxisEllipsoid(earthRadius, Constants.WGS84_EARTH_FLATTENING, ITRF);
			earthShape.setAngularThreshold(1e-12);
			earthShape.set2ndConvergenceThreshold(1e-12);
			GeodeticPoint geoCoords = earthShape.transform(orbit.getPVCoordinates(date, frame).getPosition(), frame, date);
			GeoMagneticElements fieldElements = magModel.calculateField(geoCoords);			
			Vector3D magField = fieldElements.getFieldVector().scalarMultiply(1e-9);	
			
			Frame topoFrame = new TopocentricFrame(earthShape, geoCoords, "LocalTopo");
			Transform transTopoSC = topoFrame.getTransformTo(attitude.getReferenceFrame(), date);
			Vector3D magFieldSC = transTopoSC.transformVector(magField);
			
			return Vector3D.crossProduct(direction.scalarMultiply(maxMagTorque), magFieldSC);
			
		} catch (PatriusException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getID() {
		return ID;
	}
}
