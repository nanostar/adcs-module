package adcs;

import java.util.ArrayList;

import fr.cnes.sirius.addons.patriusdataset.PatriusDataset;
import fr.cnes.sirius.patrius.frames.Frame;
import fr.cnes.sirius.patrius.frames.FramesFactory;
import fr.cnes.sirius.patrius.math.util.FastMath;
import fr.cnes.sirius.patrius.orbits.KeplerianOrbit;
import fr.cnes.sirius.patrius.orbits.PositionAngle;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.PVCoordinates;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.PVCoordinatesProvider;
import fr.cnes.sirius.patrius.time.AbsoluteDate;
import fr.cnes.sirius.patrius.time.TimeScale;
import fr.cnes.sirius.patrius.time.TimeScalesFactory;
import fr.cnes.sirius.patrius.utils.Constants;
import fr.cnes.sirius.patrius.utils.exception.PatriusException;

public class SimpleKeplerianOrbit {
	 
    public static void main(String[] args) throws PatriusException {
 
        // Patrius Dataset initialization (needed for example to get the UTC time)
        PatriusDataset.addResourcesFromPatriusDataset() ;
 
        // Recovery of the UTC time scale using a "factory" (not to duplicate such unique object)
        final TimeScale UTC = TimeScalesFactory.getUTC();
 
        // Getting the frame with wich will defined the orbit parameters
        // As for time scale, we will use also a "factory".
        final Frame EME2000 = FramesFactory.getEME2000();
 
        // Creation of a keplerian orbit
        AbsoluteDate date = new AbsoluteDate("2010-01-01T12:00:00.000", UTC);
        final double sma = 7200.e+3;
        final double exc = 0.01;
        final double inc = FastMath.toRadians(60);
        final double pa = FastMath.toRadians(0.);
        final double raan = FastMath.toRadians(0.);
        final double anm = FastMath.toRadians(0.);
        final double MU = Constants.WGS84_EARTH_MU;
        final PVCoordinatesProvider iniOrbit = new KeplerianOrbit(sma, exc, inc, pa, raan, anm, PositionAngle.MEAN, EME2000, date, MU);
 
     
        CICFile orbitEphemeris = new CICFile();       
        ArrayList<String[]> orbitEphem = new ArrayList<String[]>();        
        AbsoluteDate finalDate = date.shiftedBy(36000);
        double dt = 10;
              
        for (;date.compareTo(finalDate)<=0;date=date.shiftedBy(dt)) {        	
        	PVCoordinates pV = iniOrbit.getPVCoordinates(date, EME2000);
        	String[] ephemLine = {date.toString(),
        			Double.toString(pV.getPosition().scalarMultiply(0.001).getX()),
        			Double.toString(pV.getPosition().scalarMultiply(0.001).getY()),
        			Double.toString(pV.getPosition().scalarMultiply(0.001).getZ()),
        			Double.toString(pV.getVelocity().scalarMultiply(0.001).getX()),
        			Double.toString(pV.getVelocity().scalarMultiply(0.001).getY()),
        			Double.toString(pV.getVelocity().scalarMultiply(0.001).getZ())};
        	orbitEphem.add(ephemLine);
        }
        orbitEphemeris.setData(orbitEphem);
        orbitEphemeris.writeToFile("OEM_KEPLER1", "Data");
        


 
    }
 
}