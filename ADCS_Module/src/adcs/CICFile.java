package adcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CICFile {

	private LinkedHashMap<String,String> header = new LinkedHashMap<String,String>();
	private LinkedHashMap<String,String> metaData = new LinkedHashMap<String,String>();
	private ArrayList<String[]> data = new ArrayList<String[]>();
	
	public CICFile() {
		
	}
	
	public CICFile(String fileName, String filePath) {
			
		try {
			String key;
			String value;
			
			BufferedReader reader = Files.newBufferedReader(Paths.get(filePath + fileName));
			Pattern metaDataPattern = Pattern.compile("\\p{Space}*([A-Z][A-Z_0-9]*)\\p{Space}*=?\\p{Space}*(.*?)\\p{Space}*(?:\\[.*\\])?\\p{Space}*");
			String section = "Header";
			Matcher matcher;
			
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                
				if(line.startsWith("COMMENT") || line.trim().length() == 0) {
					continue;
				}
				
				switch (section) {
					
					case "Header":
						
						if (line.startsWith("META_START") || line.startsWith("COMMENT")){
	                		section = "MetaData";
	                		continue;
	                	}
	                	
	                	matcher = metaDataPattern.matcher(line);
	                	if (matcher.matches()) {
	                		key = matcher.group(1);
	                		value = matcher.group(2);
	                		header.put(key, value);
	                	}
						break;

					case "MetaData":
						
						if (line.contains("META_STOP")){
	                		section = "Data";
	                		continue;
	                	}
	                	
	                	matcher = metaDataPattern.matcher(line);
	                	if (matcher.matches()) {
	                		key = matcher.group(1);
	                		value = matcher.group(2);
	                		metaData.put(key, value);
	                	}
						break;
						
					case "Data":	
						String[] dataLine = line.split("\\s+");
	                	data.add(dataLine);
	                	break;
					default:
						break;
                }
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public void writeToFile(String fileName, String filePath) {

		try {
			
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filePath,fileName)));
					
			for (Map.Entry<String, String> entry : header.entrySet()) {
				writer.write(entry.getKey());
				writer.write(" = ");
				writer.write(entry.getValue());
				writer.newLine();		
			}
			
			writer.newLine();
			writer.write("META_START");
			writer.newLine();
			
			for (Map.Entry<String, String> entry : metaData.entrySet()) {
				writer.write(entry.getKey());
				writer.write(" = ");
				writer.write(entry.getValue());
				writer.newLine();		
			}
			
			writer.write("META_STOP");
			writer.newLine();
			writer.newLine();
			
			for (String[] line : data) {
				for (int i=0; i<line.length; i++) {
					writer.write(line[i]);
					writer.write("	");
				}				
				writer.newLine();
			}
			
			writer.flush();
			writer.close();
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public LinkedHashMap<String, String> getHeader() {
		return header;
	}

	public void setHeader(LinkedHashMap<String, String> header) {
		this.header = header;
	}
	
	public LinkedHashMap<String, String> getMetaData() {
		return metaData;
	}

	public void setMetaData(LinkedHashMap<String, String> metaData) {
		this.metaData = metaData;
	}

	public ArrayList<String[]> getData() {
		return data;
	}

	public void setData(ArrayList<String[]> data) {
		this.data = data;
	}
}
