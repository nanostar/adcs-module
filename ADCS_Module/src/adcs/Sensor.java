package adcs;

import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.Vector3D;

public class Sensor {
	
	private String ID;
	private double sigma;
	private Vector3D direction;
	
	public Sensor(String ID, String dirX, String dirY, String dirZ, String sigma) {
		this.ID = ID;
		direction = new Vector3D(Double.parseDouble(dirX),
				Double.parseDouble(dirY),
				Double.parseDouble(dirZ)).normalize();
		this.sigma = Double.parseDouble(sigma);
	}

	public String getID() {
		return ID;
	}

	public double getSigma() {
		return sigma;
	}

	public Vector3D getDirection() {
		return direction;
	}
}
