package adcs;

import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.Vector3D;

public class InertiaWheel {
	
	private String ID;
	private Vector3D direction;
	private double maxMoment;
	private double maxTorque;
	
	public InertiaWheel(String ID, String dirX, String dirY, String dirZ, String maxMoment, String maxTorque) {
		this.ID = ID;
		direction = new Vector3D(Double.parseDouble(dirX),
				Double.parseDouble(dirY),
				Double.parseDouble(dirZ)).normalize();
		this.maxMoment = Double.parseDouble(maxMoment);
		this.maxTorque = Double.parseDouble(maxTorque);		
	}

	public Vector3D getMaxMoment() {
		return direction.scalarMultiply(maxMoment);
	}

	public Vector3D getMaxTorque() {
		return direction.scalarMultiply(maxTorque);
	}

	public String getID() {
		return ID;
	}

}
