package adcs;

import fr.cnes.sirius.patrius.bodies.CelestialBodyFactory;
import fr.cnes.sirius.patrius.frames.Frame;
import fr.cnes.sirius.patrius.frames.FramesFactory;
import fr.cnes.sirius.patrius.frames.LOFType;
import fr.cnes.sirius.patrius.frames.LocalOrbitalFrame;
import fr.cnes.sirius.patrius.frames.Predefined;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.PVCoordinatesProvider;
import fr.cnes.sirius.patrius.time.TimeScale;
import fr.cnes.sirius.patrius.time.TimeScalesFactory;
import fr.cnes.sirius.patrius.utils.exception.PatriusException;

public class FactoryUtil {

	static public Frame getTargetFrame(String targetFrame, PVCoordinatesProvider orbit, Frame orbitFrame) {
		try {
			switch (targetFrame) {

			case "LVLH":
				return new LocalOrbitalFrame(orbitFrame, LOFType.LVLH, orbit, "LVLH");
			case "QSW":
				return new LocalOrbitalFrame(orbitFrame, LOFType.QSW, orbit, "QSW");
			case "TNW":
				return new LocalOrbitalFrame(orbitFrame, LOFType.TNW, orbit, "TNW");
			case "VNC":
				return new LocalOrbitalFrame(orbitFrame, LOFType.VNC, orbit, "VNC");
			default:
				return FramesFactory.getFrame(Predefined.valueOf(targetFrame));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	static public PVCoordinatesProvider getTargetProvider(String target) {
		try {
			switch (target) {
			case "SUN":
				return CelestialBodyFactory.getSun();
			case "EARTH":
				return CelestialBodyFactory.getEarth();
			case "EARTH_MOON":
				return CelestialBodyFactory.getEarthMoonBarycenter();
			case "JUPITER":
				return CelestialBodyFactory.getJupiter();
			case "MARS":
				return CelestialBodyFactory.getMars();
			case "MERCURY":
				return CelestialBodyFactory.getMercury();
			case "MOON":
				return CelestialBodyFactory.getMoon();
			case "NEPTUNE":
				return CelestialBodyFactory.getNeptune();
			case "PLUTO":
				return CelestialBodyFactory.getPluto();
			case "SATURN":
				return CelestialBodyFactory.getSaturn();
			case "SOLAR_SYSTEM_BARYCENTER":
				return CelestialBodyFactory.getSolarSystemBarycenter();
			case "URANUS":
				return CelestialBodyFactory.getUranus();
			case "VENUS":
				return CelestialBodyFactory.getVenus();
			}
		} catch (PatriusException e) {
			e.printStackTrace();

		}
		return null;
	}
	
	static public TimeScale getTimeScale(String timeScale) {
		try {
			switch (timeScale) {
			case "UTC":
				return TimeScalesFactory.getUTC();
			case "GMST":
				return TimeScalesFactory.getGMST();
			case "GPS":
				return TimeScalesFactory.getGPS();
			case "GST":
				return TimeScalesFactory.getGST();
			case "TAI":
				return TimeScalesFactory.getTAI();
			case "TCG":
				return TimeScalesFactory.getTCG();
			case "TCB":
				return TimeScalesFactory.getTCB();
			case "TDB":
				return TimeScalesFactory.getTDB();
			case "TT":
				return TimeScalesFactory.getTT();
			case "UT1":
				return TimeScalesFactory.getUT1();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
