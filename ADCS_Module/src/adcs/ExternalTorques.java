package adcs;

import java.util.LinkedHashMap;

import fr.cnes.sirius.patrius.assembly.models.IInertiaModel;
import fr.cnes.sirius.patrius.assembly.models.InertiaSimpleModel;
import fr.cnes.sirius.patrius.attitudes.Attitude;
import fr.cnes.sirius.patrius.attitudes.directions.GenericTargetDirection;
import fr.cnes.sirius.patrius.bodies.CelestialBodyFactory;
import fr.cnes.sirius.patrius.bodies.GeodeticPoint;
import fr.cnes.sirius.patrius.bodies.OneAxisEllipsoid;
import fr.cnes.sirius.patrius.forces.atmospheres.Atmosphere;
import fr.cnes.sirius.patrius.forces.atmospheres.HarrisPriester;
import fr.cnes.sirius.patrius.frames.Frame;
import fr.cnes.sirius.patrius.frames.FramesFactory;
import fr.cnes.sirius.patrius.frames.TopocentricFrame;
import fr.cnes.sirius.patrius.frames.transformations.Transform;
import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.Matrix3D;
import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.Vector3D;
import fr.cnes.sirius.patrius.math.linear.DiagonalMatrix;
import fr.cnes.sirius.patrius.math.linear.RealMatrix;
import fr.cnes.sirius.patrius.models.earth.GeoMagneticElements;
import fr.cnes.sirius.patrius.models.earth.GeoMagneticField;
import fr.cnes.sirius.patrius.models.earth.GeoMagneticFieldFactory;
import fr.cnes.sirius.patrius.orbits.CartesianOrbit;
import fr.cnes.sirius.patrius.orbits.Orbit;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.PVCoordinatesProvider;
import fr.cnes.sirius.patrius.propagation.SpacecraftState;
import fr.cnes.sirius.patrius.time.AbsoluteDate;
import fr.cnes.sirius.patrius.utils.Constants;
import fr.cnes.sirius.patrius.utils.exception.PatriusException;
import fr.cnes.sirius.patrius.wrenches.GravitationalAttractionWrench;

public class ExternalTorques {
	
	private double totalMass;
	private Vector3D massCenter;
	private Matrix3D inertiaMatrix;
	private Vector3D aeroCenter;
	private Vector3D aeroCoeffs;
	private Vector3D magDipole;
	private Vector3D radCenter;
	private Vector3D radForceCoeffs;
	private PVCoordinatesProvider orbit;
	private Frame frame;
	private AbsoluteDate date;
	private Attitude attitude;
	private GeoMagneticField magModel;
	
	public ExternalTorques(CICFile torqueCoeffs, CICFile inertialModel) {
		
		LinkedHashMap<String, String> values = torqueCoeffs.getMetaData();
		LinkedHashMap<String, String> model = inertialModel.getMetaData();
		
		totalMass = Double.parseDouble(model.get("TOTAL_MASS"));
		
		double[][] matrix = new double[3][3];
		for (int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				matrix[i][j] = Double.parseDouble(inertialModel.getData().get(i)[j+1]);
			}
		}
		inertiaMatrix = new Matrix3D(matrix);
				
		massCenter = new Vector3D(Double.parseDouble(model.get("MASS_CENTER_X")),
				Double.parseDouble(model.get("MASS_CENTER_Y")),
				Double.parseDouble(model.get("MASS_CENTER_Z")));
		
		aeroCenter = new Vector3D(Double.parseDouble(values.get("AERO_CENTER_X")),
				Double.parseDouble(values.get("AERO_CENTER_Y")),
				Double.parseDouble(values.get("AERO_CENTER_Z")));
		
		aeroCoeffs = new Vector3D(Double.parseDouble(values.get("AERO_CX_S")),
				Double.parseDouble(values.get("AERO_CY_S")),
				Double.parseDouble(values.get("AERO_CZ_S")));
		
		magDipole = new Vector3D(Double.parseDouble(values.get("MAG_DIPOLE_X")),
				Double.parseDouble(values.get("MAG_DIPOLE_Y")),
				Double.parseDouble(values.get("MAG_DIPOLE_Z")));
		
		radCenter = new Vector3D(Double.parseDouble(values.get("RAD_PRESSURE_CENTER_X")),
				Double.parseDouble(values.get("RAD_PRESSURE_CENTER_Y")),
				Double.parseDouble(values.get("RAD_PRESSURE_CENTER_Z")));
		
		radForceCoeffs = new Vector3D(Double.parseDouble(values.get("RAD_FORCE_X")),
				Double.parseDouble(values.get("RAD_FORCE_Y")),
				Double.parseDouble(values.get("RAD_FORCE_Z")));
		
		try {
			magModel = GeoMagneticFieldFactory.getIGRF(2018);
		} catch (PatriusException e) {
			e.printStackTrace();
		}
	}
	
	public void setState(PVCoordinatesProvider orbit, Frame frame, AbsoluteDate date, Attitude attitude) {
		this.orbit = orbit;
		this.frame = frame;
		this.date = date;
		this.attitude = attitude;
	}
	
	public Vector3D getAeroTorque() {
		try {
			Vector3D velocitySC = attitude.getRotation().applyInverseTo(orbit.getPVCoordinates(date, frame).getVelocity());
			
			Frame ITRF = FramesFactory.getITRF();
			double earthRadius = Constants.WGS84_EARTH_EQUATORIAL_RADIUS;
			OneAxisEllipsoid earthShape = new OneAxisEllipsoid(earthRadius, Constants.WGS84_EARTH_FLATTENING, ITRF);			
			Atmosphere atmosphere = new HarrisPriester(CelestialBodyFactory.getSun(), earthShape);
			
			Vector3D atmosphereVelocity = atmosphere.getVelocity(date, orbit.getPVCoordinates(date, frame).getPosition(), frame);
			Vector3D atmosphereVelocitySC = attitude.getRotation().applyInverseTo(atmosphereVelocity);
			Vector3D relativeAirVelocity = velocitySC.subtract(atmosphereVelocitySC);
			
			RealMatrix aeroCoeffsMatrix = new DiagonalMatrix(aeroCoeffs.toArray());
			
			Vector3D dynamicPressure = relativeAirVelocity.scalarMultiply(-relativeAirVelocity.getNorm()*
					0.5*atmosphere.getDensity(date, orbit.getPVCoordinates(date, frame).getPosition(), frame));
		
			Vector3D dragForce = new Vector3D(aeroCoeffsMatrix.operate(dynamicPressure.toArray()));
			
			return aeroCenter.subtract(massCenter).crossProduct(dragForce);
			
		} catch (PatriusException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Vector3D getMagTorque() {				
		try {			
			Frame ITRF = FramesFactory.getITRF();
			double earthRadius = Constants.WGS84_EARTH_EQUATORIAL_RADIUS;
			OneAxisEllipsoid earthShape = new OneAxisEllipsoid(earthRadius, Constants.WGS84_EARTH_FLATTENING, ITRF);
			earthShape.setAngularThreshold(1e-12);
			earthShape.set2ndConvergenceThreshold(1e-12);
			GeodeticPoint geoCoords = earthShape.transform(orbit.getPVCoordinates(date, frame).getPosition(), frame, date);
			GeoMagneticElements fieldElements = magModel.calculateField(geoCoords);			
			Vector3D magField = fieldElements.getFieldVector().scalarMultiply(1e-9);	
			
			Frame topoFrame = new TopocentricFrame(earthShape, geoCoords, "LocalTopo");
			Transform transTopoOemFrame = topoFrame.getTransformTo(attitude.getReferenceFrame(), date);			
			Vector3D magFieldOemFrame = transTopoOemFrame.transformVector(magField);
			Vector3D magFieldSC = attitude.getRotation().applyInverseTo(magFieldOemFrame);
			
			return magDipole.crossProduct(magFieldSC);
			
		} catch (PatriusException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Vector3D getRadTorque() {
			PVCoordinatesProvider sun = FactoryUtil.getTargetProvider("SUN");
			try {
				Vector3D sunDirectionOemFrame = new GenericTargetDirection(sun).getVector(orbit, date, frame).normalize();
				Vector3D sunDirectionSC = attitude.getRotation().applyInverseTo(sunDirectionOemFrame);
				
				RealMatrix radCoeffsMatrix = new DiagonalMatrix(radForceCoeffs.toArray());
				
				Vector3D radForce = new Vector3D(radCoeffsMatrix.operate(sunDirectionSC.toArray())).scalarMultiply(-1);
				
				return radCenter.subtract(massCenter).crossProduct(radForce.scalarMultiply(1e-6));
				
			} catch (PatriusException e) {
				e.printStackTrace();
				return null;
			}		
	}
	
	public Vector3D getGravTorque() {
	 	
		try {
			Orbit spacecraftOrbit = new CartesianOrbit(orbit.getPVCoordinates(date, frame), frame, date, Constants.WGS84_EARTH_MU);
			SpacecraftState spacecraftState = new SpacecraftState(spacecraftOrbit, attitude);
			Frame spacecraftFrame = new Frame(frame, spacecraftState.toTransform(), "SC_FRAME");			
			IInertiaModel inertialModel = new InertiaSimpleModel(totalMass, massCenter, inertiaMatrix, spacecraftFrame,"INERTIAL_MODEL");
			GravitationalAttractionWrench gravModel = new GravitationalAttractionWrench(inertialModel, Constants.WGS84_EARTH_MU);
			
			return gravModel.computeTorque(spacecraftState);
			
		} catch (PatriusException e) {
			e.printStackTrace();
			return null;
		}
	}
}
