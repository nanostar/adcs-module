package adcs;

import java.util.LinkedHashMap;

import fr.cnes.sirius.patrius.attitudes.Attitude;
import fr.cnes.sirius.patrius.attitudes.AttitudeLaw;
import fr.cnes.sirius.patrius.attitudes.ConstantAttitudeLaw;
import fr.cnes.sirius.patrius.attitudes.SpinStabilized;
import fr.cnes.sirius.patrius.attitudes.TwoDirectionsAttitude;
import fr.cnes.sirius.patrius.attitudes.directions.ConstantVectorDirection;
import fr.cnes.sirius.patrius.attitudes.directions.GenericTargetDirection;
import fr.cnes.sirius.patrius.attitudes.directions.IDirection;
import fr.cnes.sirius.patrius.bodies.ExtendedOneAxisEllipsoid;
import fr.cnes.sirius.patrius.bodies.GeodeticPoint;
import fr.cnes.sirius.patrius.bodies.GeometricBodyShape;
import fr.cnes.sirius.patrius.frames.Frame;
import fr.cnes.sirius.patrius.frames.FramesFactory;
import fr.cnes.sirius.patrius.frames.TopocentricFrame;
import fr.cnes.sirius.patrius.math.geometry.euclidean.threed.Vector3D;
import fr.cnes.sirius.patrius.math.util.FastMath;
import fr.cnes.sirius.patrius.orbits.pvcoordinates.PVCoordinatesProvider;
import fr.cnes.sirius.patrius.utils.Constants;
import fr.cnes.sirius.patrius.utils.exception.PatriusException;

public class AttitudeRequest {
	
	@SuppressWarnings("unused")
	private String modeName;
	private AttitudeLaw attitudeLaw;
	private String primaryType;
	private String secondaryType;
	private Vector3D scPrimaryAxis;
	private Vector3D scSecondaryAxis;
	private IDirection primaryTargetDirection;
	private IDirection secondaryTargetDirection;
	private boolean spinStabilized;
	private double spinRate;
	
	public AttitudeRequest(CICFile modeDefinitionFile, PVCoordinatesProvider orbit, Frame orbitFrame) throws PatriusException {
		
		LinkedHashMap<String, String> parameters = modeDefinitionFile.getMetaData();
		modeName = parameters.get("MODE_NAME");
		
		primaryType= parameters.get("PRIMARY_LAW_TYPE");	
		scPrimaryAxis = new Vector3D(Double.parseDouble(parameters.get("SPACECRAFT_PRIMARY_TARGET_AXIS_X")),
				Double.parseDouble(parameters.get("SPACECRAFT_PRIMARY_TARGET_AXIS_Y")),
				Double.parseDouble(parameters.get("SPACECRAFT_PRIMARY_TARGET_AXIS_Z")));
		
		spinStabilized = primaryType.contentEquals("SPIN_STABILIZED");
		
		if(spinStabilized) {
			spinRate = Double.parseDouble(parameters.get("SPIN_RATE"));
			secondaryType = null;
			scSecondaryAxis = null;
		} else {		
			secondaryType= parameters.get("SECONDARY_LAW_TYPE");
			scSecondaryAxis = new Vector3D(Double.parseDouble(parameters.get("SPACECRAFT_SECONDARY_TARGET_AXIS_X")),
				Double.parseDouble(parameters.get("SPACECRAFT_SECONDARY_TARGET_AXIS_Y")),
				Double.parseDouble(parameters.get("SPACECRAFT_SECONDARY_TARGET_AXIS_Z")));
		}

		switch (primaryType) {
		case "GROUND_POINTING":
			
			GeodeticPoint targetGeo = new GeodeticPoint(FastMath.toRadians(Double.parseDouble(parameters.get("PRIMARY_TARGET_LAT"))),
					FastMath.toRadians(Double.parseDouble(parameters.get("PRIMARY_TARGET_LON"))),
					Double.parseDouble(parameters.get("PRIMARY_TARGET_ALT")));

			Frame ITRF = FramesFactory.getITRF();
			double earthRadius = Constants.WGS84_EARTH_EQUATORIAL_RADIUS;
			GeometricBodyShape bodyShape = new ExtendedOneAxisEllipsoid(earthRadius, Constants.WGS84_EARTH_FLATTENING, ITRF, "EARTH");

			TopocentricFrame targetFrame = new TopocentricFrame(bodyShape, targetGeo, "TargetFrame");				
			primaryTargetDirection = new GenericTargetDirection(targetFrame);
			break;

		case "BODY_POINTING":

			PVCoordinatesProvider primaryTarget = FactoryUtil.getTargetProvider(parameters.get("PRIMARY_TARGET"));
			primaryTargetDirection = new GenericTargetDirection(primaryTarget);
			break;

		case "CONSTANT_POINTING":
			Vector3D directionVector = new Vector3D(Double.parseDouble(parameters.get("PRIMARY_TARGET_X")),
					Double.parseDouble(parameters.get("PRIMARY_TARGET_Y")),
					Double.parseDouble(parameters.get("PRIMARY_TARGET_Z")));			
			Frame directionFrame = FactoryUtil.getTargetFrame(parameters.get("PRIMARY_TARGET_FRAME"), orbit, orbitFrame);				
			primaryTargetDirection = new ConstantVectorDirection(directionVector, directionFrame);		
			break;

		}
		
		if(secondaryType != null) {
			switch (secondaryType) {
			case "GROUND_POINTING": 

				GeodeticPoint targetGeo = new GeodeticPoint(FastMath.toRadians(Double.parseDouble(parameters.get("SECONDARY_TARGET_LAT"))),
						FastMath.toRadians(Double.parseDouble(parameters.get("SECONDARY_TARGET_LON"))),
						Double.parseDouble(parameters.get("SECONDARY_TARGET_ALT")));

				Frame ITRF = FramesFactory.getITRF();
				double earthRadius = Constants.WGS84_EARTH_EQUATORIAL_RADIUS;
				GeometricBodyShape bodyShape = new ExtendedOneAxisEllipsoid(earthRadius, Constants.WGS84_EARTH_FLATTENING, ITRF, "EARTH");

				TopocentricFrame targetFrame = new TopocentricFrame(bodyShape, targetGeo, "TargetFrame");				
				secondaryTargetDirection = new GenericTargetDirection(targetFrame);

				attitudeLaw = new TwoDirectionsAttitude(primaryTargetDirection, secondaryTargetDirection, scPrimaryAxis, scSecondaryAxis);
				break;

			case "BODY_POINTING":

				PVCoordinatesProvider primaryTarget = FactoryUtil.getTargetProvider(parameters.get("SECONDARY_TARGET"));
				secondaryTargetDirection = new GenericTargetDirection(primaryTarget);
				attitudeLaw = new TwoDirectionsAttitude(primaryTargetDirection, secondaryTargetDirection, scPrimaryAxis, scSecondaryAxis);
				break;

			case "CONSTANT_POINTING":
				Vector3D directionVector = new Vector3D(Double.parseDouble(parameters.get("SECONDARY_TARGET_X")),
						Double.parseDouble(parameters.get("SECONDARY_TARGET_Y")),
						Double.parseDouble(parameters.get("SECONDARY_TARGET_Z")));
				
				Frame directionFrame = FactoryUtil.getTargetFrame(parameters.get("SECONDARY_TARGET_FRAME"), orbit, orbitFrame);	
				
				secondaryTargetDirection = new ConstantVectorDirection(directionVector, directionFrame);
				attitudeLaw = new TwoDirectionsAttitude(primaryTargetDirection, secondaryTargetDirection, scPrimaryAxis, scSecondaryAxis);
				break;
			}
		}
	}
	
	public AttitudeLaw getAttitudeLaw(Attitude currentAttitude) {
		
		if (spinStabilized) {
			AttitudeLaw currentAttitudeLaw = new ConstantAttitudeLaw(currentAttitude.getReferenceFrame(), currentAttitude.getRotation());
			attitudeLaw = new SpinStabilized(currentAttitudeLaw, currentAttitude.getDate(), scPrimaryAxis, spinRate);			
		} else {
			attitudeLaw = new TwoDirectionsAttitude(primaryTargetDirection, secondaryTargetDirection, scPrimaryAxis, scSecondaryAxis);
		}

		return attitudeLaw;
	}
}
