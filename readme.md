# NSS - ADCS Module (Attitude Determination & Control Systems)

This module shall be used to evaluate the control feasibility of a satellite mission taking into account its orbital parameters, activity profile, stabilization requirements, actuator and sensor characteristics.

This module consists of a single script that outputs an AEM file (Attitude Ephemeris Message) for a given input orbit expressed in a OEM file (Orbit Ephemeris Message) and a given input Activity Profile, expressed in MPM and MEM files (Mission Ephemeris Message, Mission Parameter Message). These AEM files may later be used as control inputs for satellite maneuvers in a 6DOF simulator to produce an accurate attitude response. The output AEM and OEM Files may also be visualised using VTS.

The feasibility study consists in analysing the parameters of on-board actuators and sensors (performance, geometry, uncertainty) and checking if the current configuration is capable of achieving the mission requirements. This module may serve as a design tool in the phase 0 of a satellite mission.

Check the Doc folder to read the full documentation.

#Dependencies

* CNES Patrius V4.3 https://logiciels.cnes.fr/fr/node/61?type=desc

This module is built using CNES Patrius as a java library for space and flight dynamics computations. This library is exported directly into the runnable NSS_ADCS.jar file avoiding the need to download the library before using the module.

#Quickstart guide

To run the module make sure you have JavaSE-11 installed.

```bash
sudo apt install openjdk-11-jre-headless
```

Clone the repository and run:

```bash
git clone https://sourceforge.isae.fr/git/nss_adcs
cd DEMO
java -jar NSS_ADCS.jar NSS_ADCS_CONFIG.txt
```

#Build

The .jar file is exported from the java project in the folder "ADCS_Module" using Eclipse IDE version 4.11.0:

https://www.eclipse.org/downloads/

##Import Project

Open Eclipse with the git repository as the workspace:

File > Import > General > Existing Projects into Workspace

Select the "ADCS_Module" folder as root directory and finish.

All referenced Patrius libraries should be in place.

Open and run ADCSMain.

(ADCS_Module > src > adcs > ADCSMain)

(The input config file is set as an argument in eclipse)

##Export Project

File > Export > Java > Runnable JAR File

Select options:
Launch configuration -> ADCSMain
Export destination -> ADCS_Module/NSS_ADCS.jar
Library handling -> Extract required libraries into generated JAR

(Skip and ignore warnings)

Copy the file to "DEMO" folder or run in place

#Files

```
.
├── ADCS_Module
│   ├── bin
│   │   └── adcs
│   │       ├── ADCSMain.class
│   │       ├── AttitudeRequest.class
│   │       ├── CICFile.class
│   │       ├── ExternalTorques.class
│   │       ├── FactoryUtil.class
│   │       ├── InertiaWheel.class
│   │       ├── Magnetorquer.class
│   │       ├── SatelliteMode.class
│   │       ├── Sensor.class
│   │       └── SimpleKeplerianOrbit.class
│   ├── Data
│   │   ├── AEM_OUT.aem
│   │   ├── DEBUG.mem
│   │   ├── NSSat_ACTIVITY_PROFILE.mem
│   │   ├── NSSat_ACTUATORS.mpm
│   │   ├── NSSat_MODE_CHARGING.mpm
│   │   ├── NSSat_MODE_CHARGING_POINTING.mpm
│   │   ├── NSSat_MODE_EARTH_OBSERVATION.mpm
│   │   ├── NSSat_MODE_TRANSMISSION.mpm
│   │   ├── NSSat_SC_INERTIAL_MODEL.mpm
│   │   ├── NSSat_SENSORS.mpm
│   │   ├── NSSat_TORQUES.mpm
│   │   ├── OEM_KEPLER.oem
│   │   └── SENSOR_TRUST_OUT.mpm
│   ├── lib
│   │   ├── patrius-4.3.jar
│   │   ├── patrius-4.3-javadoc.jar
│   │   ├── patrius-4.3.pom
│   │   ├── patrius-4.3-sources.jar
│   │   ├── patriusdataset-1.1.0.jar
│   │   ├── patriusdataset-1.1.0-javadoc.jar
│   │   └── patriusdataset-1.1.0-sources.jar
│   ├── NSS_ADCS_CONFIG.txt
│   ├── NSS_ADCS.jar
│   └── src
│       └── adcs
│           ├── ADCSMain.java
│           ├── AttitudeRequest.java
│           ├── CICFile.java
│           ├── ExternalTorques.java
│           ├── FactoryUtil.java
│           ├── InertiaWheel.java
│           ├── Magnetorquer.java
│           ├── SatelliteMode.java
│           ├── Sensor.java
│           └── SimpleKeplerianOrbit.java
├── DEMO
│   ├── Data
│   │   ├── AEM_OUT.aem
│   │   ├── DEBUG.mem
│   │   ├── NSSat_ACTIVITY_PROFILE.mem
│   │   ├── NSSat_ACTUATORS.mpm
│   │   ├── NSSat_MODE_CHARGING.mpm
│   │   ├── NSSat_MODE_CHARGING_POINTING.mpm
│   │   ├── NSSat_MODE_EARTH_OBSERVATION.mpm
│   │   ├── NSSat_MODE_TRANSMISSION.mpm
│   │   ├── NSSat_SC_INERTIAL_MODEL.mpm
│   │   ├── NSSat_SENSORS.mpm
│   │   ├── NSSat_TORQUES.mpm
│   │   ├── OEM_KEPLER.oem
│   │   └── SENSOR_TRUST_OUT.mpm
│   ├── NSS_ADCS_CONFIG.txt
│   └── NSS_ADCS.jar
├── Doc
│   └── NSS_ADCS_Doc.pdf
└── readme.md


```
